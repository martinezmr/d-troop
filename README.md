# D Troop

This repository was created for network training purposes on EVE-NG.

### Starting Off

1. Ensure you have access to Confluence.  If you don't you will need to create a forcenex.us account or have your account unlocked by contacting the #help channel in Rocketchat.  You will need to be able to access the following links:

```
https://www.wireguard.com/install/

https://confluence.forcenex.us/display/NOSS/Virtual+Lab+Tracker

https://confluence.forcenex.us/display/NOSS/Virtual+Lab+Configurations

https://confluence.forcenex.us/display/NOSS/EVE-NG

```

2. To start off, you will need to download WireGuard.  WireGuard is the VPN used to connect to the AWS cloud environment that hosts the EVE-NG labs.

3. Next, you'll need to follow the Virtual Lab Tracker link and add your name to an available lab.  The IPs in the table are what you'll type in your web browser to access your lab once your VPN is connected.

4. Then, you'll need to go to the Virtual Lab Configurations page.  Click on your student lab number and paste the contents of that file in a text document on your desktop.  Label that document ' student#.conf ' and make sure the file format changed.  You'll need this for WireGuard.

5. Lastly, start WireGuard.  Click on ' Add Tunnel '.  Click on the .conf file on your desktop.  Now, click activate. You will be connected to an Ubuntu VM hosting EVE-NG.  Use the IP associated with your student lab on the Virtual Lab Tracker to access your EVE-NG enviroment.

```
User: admin
Password: eve

```

6. Follow the EVE-NG link to give you a run through on how to navigate the lab environment.

**EXTRA

If you want to install EVE-NG locally, follow this link to walk through the process:
```
https://confluence.forcenex.us/display/JNA/2020/03/30/30+MAR+20+-+Mike+-+EVE-NG+initial+setup
```
