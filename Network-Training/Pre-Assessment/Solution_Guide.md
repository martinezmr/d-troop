# Solution_Guide

1. Configure VLANs 10(DATA1), 20 (DATA2), and 1000(MGMT) on all layer 2 devices.  Do a ' sh vlan brief ' to verify.
###### AS5, AS6, DS3, DS4, CS3, CS4
```
conf t
!
vlan 10
name DATA1
!
vlan 20
name DATA2
!
vlan 1000
name MGMT
!
end
```
2. Configures SVIs on the core switches using the second to last useable IP for CS3 and third to last useable IP for CS4.  Do a ' sh ip int brief ' to verify.
###### CS3
```
conf t
!
int vlan 10
desc DATA1
ip add 10.20.10.253 255.255.255.0
no shut
!
int vlan 20
desc DATA2
ip add 10.20.20.253 255.255.255.0
no shut
!
int vlan 1000
desc MGMT
ip add 10.20.100.253 255.255.255.0
no shut
!
end
```
###### CS4
```
conf t
!
int vlan 10
desc DATA1
ip add 10.20.10.252 255.255.255.0
no shut
!
int vlan 20
desc DATA2
ip add 10.20.20.252 255.255.255.0
no shut
!
int vlan 1000
desc MGMT
ip add 10.20.100.252 255.255.255.0
no shut
!
end
```
3. Configure HSRPv2 on CS3 and CS4 using the last usable IP in each VLAN as the virtual address.  Make CS3 primary by adjusting priority and make sure it is the active device as long as CS3 is up.  (After thought, but this won't work until the trunks are configured-- verify with 'sh standby brief')
###### CS3
```
conf t
!
int vlan 10
standby version 2
standby 10 ip 10.20.10.254
standby 10 priority 105
standby 10 preempt
!
int vlan 20
standby version 2
standby 20 ip 10.20.20.254
standby 20 priority 105
standby 20 preempt
!
int vlan 1000
standby version 2
standby 1000 ip 10.20.100.254
standby 1000 priority 105
standby 1000 preempt
!
end
```
###### CS4
```
conf t
!
int vlan 10
standby version 2
standby 10 ip 10.20.10.254
!
int vlan 20
standby version 2
standby 20 ip 10.20.20.254
!
int vlan 1000
standby version 2
standby 1000 ip 10.20.100.254
!
end
```
4. Configure a MGMT IP on DS3, DS4, AS5, and AS6.  Configure a default gateway.  Do a ' sh ip int brief ' to verify the SVI.  (Once the trunks are created ping the gateway to verify connectivty)
###### DS3
```
conf t
!
int vlan 1000
desc MGMT
ip add 10.20.100.251 255.255.255.0
no shut
!
exit
!
ip default-gateway 10.20.100.254
!
end
```
###### DS4
```
conf t
!
int vlan 1000
desc MGMT
ip add 10.20.100.250 255.255.255.0
no shut
!
exit
!
ip default-gateway 10.20.100.254
!
end
```
###### AS5
```
conf t
!
int vlan 1000
desc MGMT
ip add 10.20.100.249 255.255.255.0
no shut
!
exit
!
ip default-gateway 10.20.100.254
!
end
```
###### AS6
```
conf t
!
int vlan 1000
desc MGMT
ip add 10.20.100.248 255.255.255.0
no shut
!
exit
!
ip default-gateway 10.20.100.254
!
end
```
5. Configure CS3 to be the root bridge for all VLANs.  Configure CS4 to be the secondary root bridge. Verify with ' show spanning-tree ' or ' show spanning-tree root '
###### CS3
```
conf t
!
spanning-tree vlan 10,20,1000 priority 0

OR

spanning-tree vlan 1-4094 priority 0
!
end
```
###### CS4
```
conf t
!
spanning-tree vlan 10,20,1000 priority 4096

OR

spanning-tree vlan 1-4094 priority 4096
!
end
```
6. Enable spanning-tree loopguard, portfast edge, portfast edge bpduguard on all layer 2 devices.  Verify with ' show spanning-tree ' or ' show spanning-tree root '
###### AS5, AS6, DS3, DS4, CS3, CS4
```
conf t
!
spanning-tree loopguard default
spanning-tree portfast edge default
spanning-tree portfast edge bpduguard default
!
end
```
7. Configure e0/0 as an access port for VLAN 10 on AS5.  Configure e0/0 as an access port for VLAN 20 on AS6.  Verfiy with ' sh vlan brief ' and see that the port is in the correct VLAN.
###### AS5
```
conf t
!
int e0/0
switchport mode access
switchport access vlan 10
!
end
```
###### AS6
```
conf t
!
int e0/0
switchport mode access
switchport access vlan 20
!
end
```
8. Configure trunks between switches.  Create VLAN 999 and label it NATIVE.  Make VLAN 999 the NATIVE vlan and do not allow vlan 1 or 999 across the trunk.  Verify changes with ' sh int trunk '.
###### AS5, AS6, DS3, DS4, CS3, CS4
```
conf t
!
vlan 999
name NATIVE
!
end
```
###### AS5
```
conf t
!
interface Ethernet0/2
description Connection to DS3
switchport trunk allowed vlan 2-998,1000-4094
switchport trunk encapsulation dot1q
switchport trunk native vlan 999
switchport mode trunk
switchport nonegotiate
!
interface Ethernet0/3
description Connection to DS4
switchport trunk allowed vlan 2-998,1000-4094
switchport trunk encapsulation dot1q
switchport trunk native vlan 999
switchport mode trunk
switchport nonegotiate
!
end
```
###### AS6
```
conf t
!
interface Ethernet0/2
description Connection to DS3
switchport trunk allowed vlan 2-998,1000-4094
switchport trunk encapsulation dot1q
switchport trunk native vlan 999
switchport mode trunk
switchport nonegotiate
!
interface Ethernet0/3
description Connection to DS4
switchport trunk allowed vlan 2-998,1000-4094
switchport trunk encapsulation dot1q
switchport trunk native vlan 999
switchport mode trunk
switchport nonegotiate
!
end
```
###### DS3
```
conf t
!
interface Ethernet1/1
description Connection to AS5
switchport trunk allowed vlan 2-998,1000-4094
switchport trunk encapsulation dot1q
switchport trunk native vlan 999
switchport mode trunk
switchport nonegotiate
!
interface Ethernet1/2
description Connection to AS6
switchport trunk allowed vlan 2-998,1000-4094
switchport trunk encapsulation dot1q
switchport trunk native vlan 999
switchport mode trunk
switchport nonegotiate
!
interface Ethernet0/1
description Connection to CS3
switchport trunk allowed vlan 2-998,1000-4094
switchport trunk encapsulation dot1q
switchport trunk native vlan 999
switchport mode trunk
switchport nonegotiate
!
interface Ethernet0/0
description Connection to CS3
switchport trunk allowed vlan 2-998,1000-4094
switchport trunk encapsulation dot1q
switchport trunk native vlan 999
switchport mode trunk
switchport nonegotiate
!
interface Ethernet0/3
description Connection to CS4
switchport trunk allowed vlan 2-998,1000-4094
switchport trunk encapsulation dot1q
switchport trunk native vlan 999
switchport mode trunk
switchport nonegotiate
!
interface Ethernet0/2
description Connection to CS4
switchport trunk allowed vlan 2-998,1000-4094
switchport trunk encapsulation dot1q
switchport trunk native vlan 999
switchport mode trunk
switchport nonegotiate
!
end
```
###### DS4
```
conf t
!
interface Ethernet1/1
description Connection to AS5
switchport trunk allowed vlan 2-998,1000-4094
switchport trunk encapsulation dot1q
switchport trunk native vlan 999
switchport mode trunk
switchport nonegotiate
!
interface Ethernet1/2
description Connection to AS6
switchport trunk allowed vlan 2-998,1000-4094
switchport trunk encapsulation dot1q
switchport trunk native vlan 999
switchport mode trunk
switchport nonegotiate
!
interface Ethernet0/1
description Connection to CS4
switchport trunk allowed vlan 2-998,1000-4094
switchport trunk encapsulation dot1q
switchport trunk native vlan 999
switchport mode trunk
switchport nonegotiate
!
interface Ethernet0/0
description Connection to CS4
switchport trunk allowed vlan 2-998,1000-4094
switchport trunk encapsulation dot1q
switchport trunk native vlan 999
switchport mode trunk
switchport nonegotiate
!
interface Ethernet0/3
description Connection to CS3
switchport trunk allowed vlan 2-998,1000-4094
switchport trunk encapsulation dot1q
switchport trunk native vlan 999
switchport mode trunk
switchport nonegotiate
!
interface Ethernet0/2
description Connection to CS3
switchport trunk allowed vlan 2-998,1000-4094
switchport trunk encapsulation dot1q
switchport trunk native vlan 999
switchport mode trunk
switchport nonegotiate
!
end
```
###### CS3
```
conf t
!
interface Ethernet1/2
description Connection to CS4
switchport trunk allowed vlan 2-998,1000-4094
switchport trunk encapsulation dot1q
switchport trunk native vlan 999
switchport mode trunk
switchport nonegotiate
!
interface Ethernet1/3
description Connection to CS4
switchport trunk allowed vlan 2-998,1000-4094
switchport trunk encapsulation dot1q
switchport trunk native vlan 999
switchport mode trunk
switchport nonegotiate
!
interface Ethernet0/1
description Connection to DS3
switchport trunk allowed vlan 2-998,1000-4094
switchport trunk encapsulation dot1q
switchport trunk native vlan 999
switchport mode trunk
switchport nonegotiate
!
interface Ethernet0/0
description Connection to DS3
switchport trunk allowed vlan 2-998,1000-4094
switchport trunk encapsulation dot1q
switchport trunk native vlan 999
switchport mode trunk
switchport nonegotiate
!
interface Ethernet0/3
description Connection to DS4
switchport trunk allowed vlan 2-998,1000-4094
switchport trunk encapsulation dot1q
switchport trunk native vlan 999
switchport mode trunk
switchport nonegotiate
!
interface Ethernet0/2
description Connection to DS4
switchport trunk allowed vlan 2-998,1000-4094
switchport trunk encapsulation dot1q
switchport trunk native vlan 999
switchport mode trunk
switchport nonegotiate
!
end
```
###### CS4
```
conf t
!
interface Ethernet1/2
description Connection to CS3
switchport trunk allowed vlan 2-998,1000-4094
switchport trunk encapsulation dot1q
switchport trunk native vlan 999
switchport mode trunk
switchport nonegotiate
!
interface Ethernet1/3
description Connection to CS3
switchport trunk allowed vlan 2-998,1000-4094
switchport trunk encapsulation dot1q
switchport trunk native vlan 999
switchport mode trunk
switchport nonegotiate
!
interface Ethernet0/1
description Connection to DS4
switchport trunk allowed vlan 2-998,1000-4094
switchport trunk encapsulation dot1q
switchport trunk native vlan 999
switchport mode trunk
switchport nonegotiate
!
interface Ethernet0/0
description Connection to DS4
switchport trunk allowed vlan 2-998,1000-4094
switchport trunk encapsulation dot1q
switchport trunk native vlan 999
switchport mode trunk
switchport nonegotiate
!
interface Ethernet0/3
description Connection to DS3
switchport trunk allowed vlan 2-998,1000-4094
switchport trunk encapsulation dot1q
switchport trunk native vlan 999
switchport mode trunk
switchport nonegotiate
!
interface Ethernet0/2
description Connection to DS3
switchport trunk allowed vlan 2-998,1000-4094
switchport trunk encapsulation dot1q
switchport trunk native vlan 999
switchport mode trunk
switchport nonegotiate
!
end
```
9. Create a layer 2 etherchannel using LACP between CS4 and CS4 using e1/2 and e1/3.  Create a layer 2 etherchannel with the remaining connections between the core switches and distribution switches.  Verify changes with ' sh etherchannel summary ' and ' sh int trunk '.  If you are having issues, do a ' sh int status ' and see if your have any err-disabled ports.
###### DS3
```
conf t
!
interface Ethernet0/1
channel-group 3 mode passive
!
interface Ethernet0/0
channel-group 3 mode passive
!
interface Ethernet0/3
channel-group 4 mode passive
!
interface Ethernet0/2
channel-group 4 mode passive
!
interface Port-channel3
description L2 Etherchannel to CS3
switchport trunk allowed vlan 2-998,1000-4094
switchport trunk encapsulation dot1q
switchport trunk native vlan 999
switchport mode trunk
switchport nonegotiate
!
interface Port-channel4
description L2 Etherchannel to CS4
switchport trunk allowed vlan 2-998,1000-4094
switchport trunk encapsulation dot1q
switchport trunk native vlan 999
switchport mode trunk
switchport nonegotiate
!
end
```
###### DS4
```
conf t
!
interface Ethernet0/1
channel-group 4 mode passive
!
interface Ethernet0/0
channel-group 4 mode passive
!
interface Ethernet0/3
channel-group 3 mode passive
!
interface Ethernet0/2
channel-group 3 mode passive
!
interface Port-channel3
description L2 Etherchannel to CS3
switchport trunk allowed vlan 2-998,1000-4094
switchport trunk encapsulation dot1q
switchport trunk native vlan 999
switchport mode trunk
switchport nonegotiate
!
interface Port-channel4
description L2 Etherchannel to CS4
switchport trunk allowed vlan 2-998,1000-4094
switchport trunk encapsulation dot1q
switchport trunk native vlan 999
switchport mode trunk
switchport nonegotiate
!
end
```
###### CS3
```
conf t
!
interface Ethernet0/1
channel-group 3 mode active
!
interface Ethernet0/0
channel-group 3 mode active
!
interface Ethernet0/3
channel-group 4 mode active
!
interface Ethernet0/2
channel-group 4 mode active
!
interface Ethernet1/2
channel-group 2 mode passive
!
interface Ethernet1/3
channel-group 2 mode passive
!
interface Port-channel2
description L2 Etherchannel to CS4
switchport trunk allowed vlan 2-998,1000-4094
switchport trunk encapsulation dot1q
switchport trunk native vlan 999
switchport mode trunk
switchport nonegotiate
!
interface Port-channel3
description L2 Etherchannel to DS3
switchport trunk allowed vlan 2-998,1000-4094
switchport trunk encapsulation dot1q
switchport trunk native vlan 999
switchport mode trunk
switchport nonegotiate
!
interface Port-channel4
description L2 Etherchannel to DS4
switchport trunk allowed vlan 2-998,1000-4094
switchport trunk encapsulation dot1q
switchport trunk native vlan 999
switchport mode trunk
switchport nonegotiate
!
end
```
###### CS4
```
conf t
!
interface Ethernet0/1
channel-group 4 mode active
!
interface Ethernet0/0
channel-group 4 mode active
!
interface Ethernet0/3
channel-group 3 mode active
!
interface Ethernet0/2
channel-group 3 mode active
!
interface Ethernet1/2
channel-group 2 mode active
!
interface Ethernet1/3
channel-group 2 mode active
!
interface Port-channel2
description L2 Etherchannel to CS3
switchport trunk allowed vlan 2-998,1000-4094
switchport trunk encapsulation dot1q
switchport trunk native vlan 999
switchport mode trunk
switchport nonegotiate
!
interface Port-channel3
description L2 Etherchannel to DS3
switchport trunk allowed vlan 2-998,1000-4094
switchport trunk encapsulation dot1q
switchport trunk native vlan 999
switchport mode trunk
switchport nonegotiate
!
interface Port-channel4
description L2 Etherchannel to DS4
switchport trunk allowed vlan 2-998,1000-4094
switchport trunk encapsulation dot1q
switchport trunk native vlan 999
switchport mode trunk
switchport nonegotiate
!
end
```
10. Configure e1/0 and e1/1 on CS3 and CS4 as layer 3 ports and create a layer 3 etherchannel using LACP.  Use subnet 10.200.200.0 255.255.255.254.  Verify with ' sh etherchannel summary '.  You'll see that the port-channel uses (RU) because its routed.  You can also verify with ' sh int status ' to see that it is a routed port.
###### CS3
```
conf t
!
interface Ethernet1/0
description Connection to CS4
no switchport
channel-group 1 mode passive
!
interface Ethernet1/1
description Connection to CS4
no switchport
channel-group 1 mode passive
!
interface Port-channel1
description L3 Etherchannel to CS4
no switchport
ip address 10.200.200.0 255.255.255.254
!
end
```
###### CS4
```
conf t
!
interface Ethernet1/0
description Connection to CS3
no switchport
channel-group 1 mode active
!
interface Ethernet1/1
description Connection to CS3
no switchport
channel-group 1 mode active
!
interface Port-channel1
description L3 Etherchannel to CS3
no switchport
ip address 10.200.200.1 255.255.255.254
!
end
```
11. Create a layer 3 connection between CS3 to R3 using subnet 10.200.200.2 255.255.255.254
###### CS3
```
conf t
!
interface Ethernet2/0
description L3 Connection to R3
no switchport
ip address 10.200.200.2 255.255.255.254
!
end
```
###### R3
```
conf t
!
interface Ethernet0/0
description L3 Connection to CS3
ip address 10.200.200.3 255.255.255.254
!
end
```
12. Create a layer 3 connection between CS3 to R4 using subnet 10.200.200.4 255.255.255.254
###### CS3
```
conf t
!
interface Ethernet2/1
description L3 Connection to R4
no switchport
ip address 10.200.200.4 255.255.255.254
!
end
```
###### R4
```
conf t
!
interface Ethernet0/1
description L3 Connection to CS3
ip address 10.200.200.5 255.255.255.254
!
end
```
13. Create a layer 3 connection between CS4 to R4 using subnet 10.200.200.6 255.255.255.254
###### CS4
```
conf t
!
interface Ethernet2/0
description L3 Connection to R4
no switchport
ip address 10.200.200.6 255.255.255.254
!
end
```
###### R4
```
conf t
!
interface Ethernet0/0
description L3 Connection to CS4
ip address 10.200.200.7 255.255.255.254
!
end
```
14. Create a layer 3 connection between CS4 to R3 using subnet 10.200.200.8 255.255.255.254
###### CS4
```
conf t
!
interface Ethernet2/1
description L3 Connection to R3
no switchport
ip address 10.200.200.8 255.255.255.254
!
end
```
###### R3
```
conf t
!
interface Ethernet0/1
description L3 Connection to CS4
ip address 10.200.200.9 255.255.255.254
!
end
```
15. Create a layer 3 connection between R3 and R4 using subnet 10.200.200.10 255.255.255.254
###### R3
```
conf t
!
interface Ethernet0/2
 description L3 Connection to R4
 ip address 10.200.200.10 255.255.255.254
!
end
```
###### R4
```
conf t
!
interface Ethernet0/2
 description L3 Connection to R4
 ip address 10.200.200.11 255.255.255.254
!
end
```
16. Add the following WAN keys to R3, R4, CS3, and CS4.
```
conf t
!
key chain Wan
 key 1
  key-string 1234567890
  accept-lifetime 00:00:00 May 1 2020 00:00:00 Jul 31 2020
  send-lifetime 00:00:00 May 1 2020 00:00:00 Jul 31 2020
 key 2
  key-string 2345678901
  accept-lifetime 00:00:00 Aug 1 2020 00:00:00 Oct 31 2020
  send-lifetime 00:00:00 Aug 1 2020 00:00:00 Oct 31 2020
 key 3
  key-string 3456789012
  accept-lifetime 00:00:00 Aug 1 2020 00:00:00 Oct 31 2020
  send-lifetime 00:00:00 Aug 1 2020 00:00:00 Oct 31 2020
 key 4
  key-string 4567890123
  accept-lifetime 00:00:00 Nov 1 2020 00:00:00 Jan 31 2021
  send-lifetime 00:00:00 Nov 1 2020 00:00:00 Jan 31 2021
 key 5
  key-string 5678901234
  accept-lifetime 00:00:00 Feb 1 2021 00:00:00 Apr 30 2021
  send-lifetime 00:00:00 Feb 1 2021 00:00:00 Apr 30 2021
 key 6
  key-string 6789012345
  accept-lifetime 00:00:00 May 1 2021 00:00:00 Jul 31 2021
  send-lifetime 00:00:00 May 1 2021 00:00:00 Jul 31 2021
 key 7
  key-string 7890123456
  accept-lifetime 00:00:00 Aug 1 2021 00:00:00 Oct 31 2021
  send-lifetime 00:00:00 Aug 1 2021 00:00:00 Oct 31 2021
 key 8
  key-string 8901234567
  accept-lifetime 00:00:00 Nov 1 2021 00:00:00 Jan 31 2022
  send-lifetime 00:00:00 Nov 1 2021 00:00:00 Jan 31 2022
 key 9
  key-string 9012345678
  accept-lifetime 00:00:00 Feb 1 2022 00:00:00 Apr 30 2022
  send-lifetime 00:00:00 Feb 1 2022 00:00:00 Apr 30 2022
 key 10
  key-string 1123456789
  accept-lifetime 00:00:00 May 1 2022 00:00:00 Jul 31 2022
  send-lifetime 00:00:00 May 1 2022 00:00:00 Jul 31 2022
!
end
```
17. Configure eigrp on CS3 and CS4.  Name it router eigrp WAN and use AS 10.  Configure an af-interface default containing authentication mode md5, key-chain, and passive-interface.  No passive the layer 3 interfaces.  Add your network to EIGRP  (use a /8 to save time).  Neighborships between the core switches should have came up.  Verify with ' sh ip eigrp neighbors '.
###### CS3, CS4
```
conf t
!
router eigrp WAN
 !
 address-family ipv4 unicast autonomous-system 10
  !
  af-interface default
   authentication mode md5
   authentication key-chain Wan
   passive-interface
  exit-af-interface
  !
  af-interface Port-channel1
   no passive-interface
  exit-af-interface
  !
  af-interface Ethernet2/0
   no passive-interface
  exit-af-interface
  !
  af-interface Ethernet2/1
   no passive-interface
  exit-af-interface
  !
  topology base
  exit-af-topology
  network 10.0.0.0
 exit-address-family
!
end
```
18. Configure DHCP pools on CS3 and CS4 for data VLANs 10 (DATA1) and 20 (DATA2) (do not worry about DNS).
###### CS3, CS4
```
conf t
!
ip dhcp pool DATA1
 network 10.20.10.0 255.255.255.0
 default-router 10.20.10.254
!
ip dhcp pool DATA2
 network 10.20.20.0 255.255.255.0
 default-router 10.20.20.254
!
end
```
19. Create a GRE tunnel from R3 to R1.  Use e0/3 as the source interface.  The tunnel is already built on R1--use that for any additional information you need.  Use the last two octets of the destination IP for the naming convention.  Don't forget your static route.  Although EIGRP isn't up, you should be able to ping across to the destination with no issues.
###### R3
```
conf t
!
interface Tunnel232
 description Tunnel to R3
 bandwidth 10000
 ip unnumbered Loopback0
 ip mtu 1400
 ip pim sparse-mode
 ip tcp adjust-mss 1320
 delay 4000
 tunnel source Ethernet0/3
 tunnel destination 192.168.11.2
exit
!
ip route 192.168.0.0 255.255.0.0 192.168.23.1
!
end
```
20. Configure eigrp on R3.  Name it router eigrp WAN and use AS 10.  Configure an af-interface default containing authentication mode md5, key-chain, and passive-interface.  No passive the layer 3 interfaces.  Add your network and loopback to EIGRP.  Verify the neighbors came up wiht the core switches and d/e tunnel with ' sh ip eigrp neighbors '.
###### R3
```
conf t
!
router eigrp WAN
 !
 address-family ipv4 unicast autonomous-system 10
  !
  af-interface default
   authentication mode md5
   authentication key-chain Wan
   passive-interface
  exit-af-interface
  !
  af-interface Tunnel232
   no passive-interface
  exit-af-interface
  !
  af-interface Ethernet0/2
   no passive-interface
  exit-af-interface
  !
  af-interface Ethernet0/1
   no passive-interface
  exit-af-interface
  !
  af-interface Ethernet0/0
   no passive-interface
  exit-af-interface
  !
  topology base
  exit-af-topology
  network 10.0.0.0
  network 3.3.3.3
 exit-address-family
!
end
```
21. Create a DMVPN SPOKE from R4 to R2.  Use e0/3 as the source interface.  The hub is already built on R2--use that for any additional information you need.  Manually assign an IP to the spoke (does not take DHCP commands).  Don't worry about applying IPSEC to encrypt the tunnel.  Although you may not have neighbors, your DMVPN should be up.  Verify with ' sh dmvpn '.  You should be able to ping to the underlay IP (192.168.10.1) as well.
###### R4
```
conf t
!
interface Tunnel169
 description SPOKE
 ip address 10.29.0.170 255.255.255.248
 no ip redirects
 ip mtu 1400
 ip nhrp map multicast 192.168.10.1
 ip nhrp map 10.29.0.169 192.168.10.1
 ip nhrp network-id 10
 ip nhrp interest none
 ip nhrp nhs 10.29.0.169
 ip tcp adjust-mss 1360
 load-interval 30
 delay 5000
 tunnel source e0/3
 tunnel mode gre multipoint
 tunnel key 112233
 tunnel protection ipsec profile IPSEC-PROFILE-DMVPN
!
ip route 10.29.0.168 255.255.255.248 tunnel169
!
end
```
22. Configure eigrp on R4.  Name it router eigrp WAN and use AS 10.  Configure an af-interface default containing authentication mode md5, key-chain, and passive-interface.  No passive the layer 3 interfaces.  Add your network and loopback to EIGRP.  Neighborship with R3, the hub router, and the core switches should have popped in.  Verify with ' sh ip eigrp neighbors '.
###### R4
```
conf t
!
router eigrp WAN
 !
 address-family ipv4 unicast autonomous-system 10
  !
  af-interface default
   authentication mode md5
   authentication key-chain Wan
   passive-interface
  exit-af-interface
  !
  af-interface Tunnel169
   no passive-interface
  exit-af-interface
  !
  af-interface Ethernet0/2
   no passive-interface
  exit-af-interface
  !
  af-interface Ethernet0/1
   no passive-interface
  exit-af-interface
  !
  af-interface Ethernet0/0
   no passive-interface
  exit-af-interface
  !
  topology base
  exit-af-topology
  network 10.0.0.0
  network 4.4.4.4
 exit-address-family
!
end
```
23. Configure a standard access-list called SSH.  Permit only the data IPs so local administrators can access the devices remotely.  Apply this to line vty 0 4.  Do this on all the CAN site devices.  Although you won't see any matches because no one is SSH'ing from those virtual PCs, you should see matches on the access-list whenever you SSH into a device.  Verify with ' sh ip access-list '.
###### AS5, AS6, DS3, DS4, CS3, CS4, R3, R4
```
conf t
!
ip access-list standard SSH
 permit 10.0.0.0 255.0.0.0
 deny any

or

ip access-list standard SSH
 permit 10.10.10.0 255.255.255.0
 permit 10.10.20.0 255.255.255.0
 permit 10.20.10.0 255.255.255.0
 permit 10.20.20.0 255.255.255.0
 deny any
!
exit
!
line vty 0 4
access-class SSH in
!
exit
!
end
```
24. Configure access-list 99 on R3.  Only allow the subnets in the network and loopback addresses out the network.  Apply it as a distribute-list out your GRE tunnel.  This will only allow those specific subnets out that tunnel.  It will cause EIGRP to restart once applied.
###### R3
```
conf t
!
ip access-list standard 99
 permit 3.3.3.3 255.255.255.255
 permit 4.4.4.4 255.255.255.255
 permit 10.22.3.3 255.255.255.255
 permit 10.22.4.4 255.255.255.255
 permit 10.20.10.0 255.255.255.0
 permit 10.20.20.0 255.255.255.0
 permit 10.20.100.0 255.255.255.0
 deny any
!
exit
!
router eigrp WAN
 !
address-family ipv4 unicast autonomous-system 10
!
topology base
!
distribute-list 99 out tunnel232
!
end
```
25. Configure access-list 99 on R4.  Only allow a default route out the network.  Apply it as a distribute-list out your SPOKE tunnel.  This will only allow those specific subnets out that tunnel.  It will cause EIGRP to restart once applied.  What makes default routes special is that the router will choose the MOST specific route in the routing table to go through.  A default route is the most unspecific subnet possible since it includes all networks in its range, therefore making it the last choice your router will use to route traffic.
###### R4
```
conf t
!
ip access-list standard 99
 permit 0.0.0.0 0.0.0.0
 deny any
!
exit
!
router eigrp WAN
 !
address-family ipv4 unicast autonomous-system 10
!
topology base
!
distribute-list 99 out Tunnel169
!
end
```
26. Enable ip multicast-routing and ' ip pim autorp listener ' on all your layer 2 and 3 devices.  Make sure your layer 3 interfaces (i.e. loopbacks and SVIs are configured for ip pim sparse mode).  Once this is done, you'll see PIM neighbors come up.  Verify you are receiving mutlicast traffic on L3 devices with ' sh ip mroute '.
###### AS5, AS6, DS3, DS4, CS3, CS4, R3, R4
```
conf t
!
ip multicast-routing
!
ip pim autorp listener
!
end
```
###### CS3
```
conf t
!
int po1
ip pim sparse-mode
!
int e2/0
ip pim sparse-mode
!
int e2/1
ip pim sparse-mode
!
int vlan 10
ip pim sparse-mode
!
int vlan 20
ip pim sparse-mode
!
int vlan 1000
ip pim sparse-mode
!
end
```
###### CS4
```
conf t
!
int po1
ip pim sparse-mode
!
int e2/0
ip pim sparse-mode
!
int e2/1
ip pim sparse-mode
!
int vlan 10
ip pim sparse-mode
!
int vlan 20
ip pim sparse-mode
!
int vlan 1000
ip pim sparse-mode
!
end
```
###### R3
```
conf t
!
int e0/0
ip pim sparse-mode
!
int e0/1
ip pim sparse-mode
!
int e0/2
ip pim sparse-mode
!
int tu232
ip pim sparse-mode
!
end
```
###### R4
```
conf t
!
int e0/0
ip pim sparse-mode
!
int e0/1
ip pim sparse-mode
!
int e0/2
ip pim sparse-mode
!
int tu169
ip pim sparse-mode
!
end
```
26. Final step, click on PC1 and PC5.  Type dhcp.  Verify they pulled IPs.  Verify connectivity between devices.  If you trace it, your ping should go through R3 to R1.
