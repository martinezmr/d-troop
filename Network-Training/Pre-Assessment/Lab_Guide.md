# Pre-Assessment

Good morning!  Time to get your hands dirty.  You've been tasked to stand up a CAN site.  The network infrastructure was stood up and the connections were made.  Unfortunately, most of the configurations were left off the devices.  Configure the CAN site following the tasks below.  You may use any resources available to you to get comms up.

1. Configure VLANs 10(DATA1), 20 (DATA2), and 1000(MGMT) on all layer 2 devices.

2. Configures SVIs on the core switches using the second to last useable IP for CS3 and third to last useable IP for CS4.

3. Configure HSRPv2 on CS3 and CS4 using the last usable IP in each VLAN as the virtual address.  Make CS3 primary by adjusting priority and make sure it is the active device as long as CS3 is up.

4. Configure a MGMT IP on DS3, DS4, AS5, and AS6.  Configure a default gateway.

5. Configure CS3 to be the root bridge for all VLANs.  Configure CS4 to be the secondary root bridge.

6. Enable spanning-tree loopguard, portfast edge, portfast edge bpduguard on all layer 2 devices.

7. Configure e0/0 as an access port for VLAN 10 on AS5.  Configure e0/0 as an access port for VLAN 20 on AS6.

8. Configure trunks between switches.  Create VLAN 999 and label it NATIVE.  Make VLAN 999 the NATIVE vlan and do not allow vlan 1 or 999 across the trunk.

9. Create a layer 2 etherchannel using LACP between CS4 and CS4 using e1/2 and e1/3.  Create a layer 2 etherchannel with the remaining connections between the core switches and distribution switches.

10. Configure e1/0 and e1/1 on CS3 and CS4 as layer 3 ports and create a layer 3 etherchannel using LACP.  Use subnet 10.200.200.0 255.255.255.254.

11. Create a layer 3 connection between CS3 to R3 using subnet 10.200.200.2 255.255.255.254

12. Create a layer 3 connection between CS3 to R4 using subnet 10.200.200.4 255.255.255.254

13. Create a layer 3 connection between CS4 to R4 using subnet 10.200.200.6 255.255.255.254

14. Create a layer 3 connection between CS4 to R3 using subnet 10.200.200.8 255.255.255.254

15. Create a layer 3 connection between R3 and R4 using subnet 10.200.200.10 255.255.255.254

16. Add the following key chains to R3, R4, CS3, and CS4.
```
key chain Wan
 key 1
  key-string 1234567890
  accept-lifetime 00:00:00 May 1 2020 00:00:00 Jul 31 2020
  send-lifetime 00:00:00 May 1 2020 00:00:00 Jul 31 2020
 key 2
  key-string 2345678901
  accept-lifetime 00:00:00 Aug 1 2020 00:00:00 Oct 31 2020
  send-lifetime 00:00:00 Aug 1 2020 00:00:00 Oct 31 2020
 key 3
  key-string 3456789012
  accept-lifetime 00:00:00 Aug 1 2020 00:00:00 Oct 31 2020
  send-lifetime 00:00:00 Aug 1 2020 00:00:00 Oct 31 2020
 key 4
  key-string 4567890123
  accept-lifetime 00:00:00 Nov 1 2020 00:00:00 Jan 31 2021
  send-lifetime 00:00:00 Nov 1 2020 00:00:00 Jan 31 2021
 key 5
  key-string 5678901234
  accept-lifetime 00:00:00 Feb 1 2021 00:00:00 Apr 30 2021
  send-lifetime 00:00:00 Feb 1 2021 00:00:00 Apr 30 2021
 key 6
  key-string 6789012345
  accept-lifetime 00:00:00 May 1 2021 00:00:00 Jul 31 2021
  send-lifetime 00:00:00 May 1 2021 00:00:00 Jul 31 2021
 key 7
  key-string 7890123456
  accept-lifetime 00:00:00 Aug 1 2021 00:00:00 Oct 31 2021
  send-lifetime 00:00:00 Aug 1 2021 00:00:00 Oct 31 2021
 key 8
  key-string 8901234567
  accept-lifetime 00:00:00 Nov 1 2021 00:00:00 Jan 31 2022
  send-lifetime 00:00:00 Nov 1 2021 00:00:00 Jan 31 2022
 key 9
  key-string 9012345678
  accept-lifetime 00:00:00 Feb 1 2022 00:00:00 Apr 30 2022
  send-lifetime 00:00:00 Feb 1 2022 00:00:00 Apr 30 2022
 key 10
  key-string 1123456789
  accept-lifetime 00:00:00 May 1 2022 00:00:00 Jul 31 2022
  send-lifetime 00:00:00 May 1 2022 00:00:00 Jul 31 2022
```
17. Configure named-mode eigrp on CS3 and CS4.  Name it router eigrp WAN and use AS 10.  Configure an af-interface default containing authentication mode md5, key-chain, and passive-interface.  No passive the layer 3 interfaces.  Add your network to EIGRP  (use a /8 to save time).

18. Configure DHCP pools on CS3 and CS4 for data VLANs 10 (DATA1) and 20 (DATA2) (do not worry about DNS).

19. Create a GRE tunnel from R3 to R1.  Use e0/3 as the source interface.  The tunnel is already built on R1--use that for any additional information you need.  Use the last two octets of the destination IP for the naming convention.  Don't forget your static route.

20. Configure named-mode eigrp on R3.  Name it router eigrp WAN and use AS 10.  Configure an af-interface default containing authentication mode md5, key-chain, and passive-interface.  No passive the layer 3 interfaces.  Add your network and loopback to EIGRP.

21. Create a DMVPN SPOKE from R4 to R2.  Use e0/3 as the source interface.  The hub is already built on R2--use that for any additional information you need.  Manually assign an IP to the spoke (does not take DHCP commands).  Don't worry about applying IPSEC to encrypt the tunnel.

22. Configure named-mode eigrp on R4.  Name it router eigrp WAN and use AS 10.  Configure an af-interface default containing authentication mode md5, key-chain, and passive-interface.  No passive the layer 3 interfaces.  Add your network and loopback to EIGRP.

23. Configure a standard access-list called SSH.  Permit only the data IPs so local administrators can access the devices remotely.  Apply this to line vty 0 4.  Do this on all the CAN site devices.

24. Configure access-list 99 on R3.  Only allow the subnets in the network and loopback addresses out the network.  Apply it as a distribute-list out your GRE tunnel.

25. Configure access-list 99 on R4.  Only allow a default route out the network.  Apply it as a distribute-list out your SPOKE tunnel.

26. Enable ip multicast-routing and ' ip pim autorp listener ' on all your layer 2 and 3 devices.  Make sure your layer 3 interfaces (i.e. loopbacks and SVIs are configured for ip pim sparse mode).

26. Final step, click on PC1 and PC5.  Type dhcp.  Verify they pulled IPs.  Verify connectivity between devices.
