What is BGP?

Border Gateway Protocol - is a standardized exterior gateway protocol designed to exchange routing and reachability information using automonous system (AS) numbers.

Like EIGRP you have to state what AS number you have for that router. 

To establish a neighbor BOTH routers need to have BGP statements pointing to each other. Like a trust system (I don't want a neighbor who doesn't want to be my neighbor)


Example Configuration
```
router bgp AS#
neighbor x.x.x.x (INTERFACE OF NEIGHBOR) remote-as (NEIGHBOR AS#)
network x.x.x.x mask x.x.x.x (To advertise your neighbors do a show ip route and see what you have directly connected)
network 1.1.1.1 mask 255.255.255.255
!
If you have a loopback of 1.1.1.1 255.255.255.255
You perform a sh ip route and see that a directly connected route for 1.1.1.1 and a mask of 255.255.255.255 IT HAS TO BE PUT LIKE THAT INTO THE BGP CONFIGURATION.
!
````



### Configure Router 1 with the following.

1) Configure the lookback0 with the IP address given.
2) Configure the interface with the given IP address.
3) Configure a hostname of R1.
4) Configure BGP for R1 with the associated AS number

### Repeat for the rest of the routers.


P.S I'm not a BGP expert, just giving out ideas on how to configure and see how BGP works. 

If you're still confused watch this video.

https://www.youtube.com/watch?v=z8INzy9E628

