### Router 1 and 2 have been preconfigured.

### HSRP
What is HSRP?
Hot Standby Routing Protocol - allowing redundancy protocol for establishing a fault-tolerant default gateway

1) Configure the following routed interfaces on CSW 1 and 2 using the IP address below.

* Core Switch 1 
```
interface e0/1
no switchport
ip address 10.22.15.2 255.255.255.254
```

* Core Switch 2
```
interface e0/1
no switchport
ip address 10.22.15.4 255.255.255.254
```

2) Reconfigure previously configured IP addresses on the SVIs to enterprise standards.
> The enterpise saves the last useable IP as the HSRP address.

* Core Switch 1

```
interface Vlan10
 ip address 22.74.15.125 255.255.255.128
!

interface Vlan16
 ip address 22.74.15.253 255.255.255.128
!

interface Vlan99
 ip address 10.22.15.141 255.255.255.240

!
interface Vlan200
 ip address 172.42.15.125 255.255.255.128

!
interface Vlan300
 ip address 10.22.15.157 255.255.255.240

!
interface Vlan1000
 ip address 10.22.15.253 255.255.255.224
```

* Core Switch 2

Enable IP routing
```
en
config t
ip routing
```

Create SVI for all the vlans and assign them an IP address to meet enterprise standards for HSRP.
```
interface Vlan10
 ip address 22.74.15.124 255.255.255.128
 no shutdown

!
interface Vlan16
 ip address 22.74.15.252 255.255.255.128
 no shutdown

!
interface Vlan99
 ip address 10.22.15.140 255.255.255.240
 no shutdown

!
interface Vlan200
 ip address 172.42.15.124 255.255.255.128
 no shutdown

!
interface Vlan300
 ip address 10.22.15.156 255.255.255.240
 no shutdown

!
interface Vlan1000
 ip address 10.22.15.252 255.255.255.224
 no shutdown
```

3) Configure EIGRP on Core Switch 1 and 2.

> Note upgrading classic EIGRP to named the commands are.

```
EXAMPLE
router eigrp 100
eigrp upgrade-cli ?
  WORD  EIGRP Virtual-Instance Name


eigrp upgrade-cli SHENANIGANS
Configuration will be converted from router eigrp 100 to router eigrp SHENANIGANS.
Are you sure you want to proceed? ? [yes/no]: yes
```

Named configuration.

* Core Switch 1
```
router eigrp SHENANIGANS
 !
 address-family ipv4 unicast autonomous-system 100
  !
  topology base
  exit-af-topology
  network 10.22.15.0 0.0.0.255
  network 22.74.15.0 0.0.0.255
  network 172.42.15.0 0.0.0.255
 exit-address-family
```

* Core Swtich 2
```
router eigrp SHENANIGANS
 !
 address-family ipv4 unicast autonomous-system 100
  !
  topology base
  exit-af-topology
  network 10.22.15.0 0.0.0.255
  network 22.74.15.0 0.0.0.255
  network 172.42.15.0 0.0.0.255
 exit-address-family
 ```


4) Type in everyone's favorite set of commands to verify neighbor relationships.
```
show ip eigrp neighbor
```

5) Configure HSRP with the following.

HSRP version 2
HSRP group numbers should match the VLAN IP number
HSRP Virtual IP should be the last useable
Core Switch should hold the active role.

* Core Switch 1
```
interface Vlan10
 standby version 2
 standby 10 ip 22.74.15.126
 standby 10 priority 150
 standby 10 preempt
!
interface Vlan16
 standby version 2
 standby 16 priority 150
 standby 16 preempt
 standby 16 ip 22.74.15.254
!
interface Vlan99
 standby version 2
 standby 99 ip 10.22.15.142
 standby 99 priority 150
 standby 99 preempt
!
interface Vlan200
 standby version 2
 standby 200 ip 172.42.15.126
 standby 200 priority 150
 standby 200 preempt
!
interface Vlan300
 standby version 2
 standby 200 preempt
 standby 300 ip 10.22.15.158
 standby 300 priority 150
!
interface Vlan1000
 standby version 2
 standby 1000 ip 10.22.15.254
 standby 1000 priority 150
 standby 1000 preempt
```

* Core Switch 2
```
interface Vlan10
 standby version 2
 standby 10 ip 10.74.15.126
!
interface Vlan16
 standby version 2
 standby 16 ip 22.74.15.254
!
interface Vlan99
 standby version 2
 standby 99 ip 10.22.15.142
!
interface Vlan200
 standby version 2
 standby 200 ip 172.42.15.126
!
interface Vlan300
 standby version 2
 standby 300 ip 10.22.15.158
!
interface Vlan1000
 standby version 2
 standby 1000 ip 10.22.15.254
```

Verify HSRP operation on Core Switch 1 and 2.
```
show standby brief
```