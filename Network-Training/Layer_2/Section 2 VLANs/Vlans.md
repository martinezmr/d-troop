### Configure the following VLANs on ALL switches.



DATA - 10

SERVERS - 16

PRINTERS - 99

VOICE - 200

VIDEO - 300

MANAGEMENT - 1000

```
vlan 10
name DATA

!
vlan 16
name SERVER

!
vlan 99
name PRINTER

!
vlan 200
name Voice

!
vlan 300
name VIDEO

!
vlan 1000
name MGMT
```

## Configure the following ports
> Most Phones have a laptop hanging off of them. In this case we'll assume there is a phone in front of the PC.

* Switch 1

```
en
config t
interface ethernet 0/1
description VOICE/DATA
switchport access vlan 10
switchport voice vlan 200
switchport mode access

!
interface ethernet 0/2
description PRINTER
switchport access vlan 99
switchport mode access
```

* Switch 2

Vipers are configured with a switchport access vlan 200.
> NOT VOICE VLAN 200.

```
en
config t
interface ethernet 0/1
description VIPER
switchport access vlan 200
switchport mode access

!
interface ethernet 0/2
description VTC
switchport access vlan 300
switchport mode access

!
interface ethernet 0/3
description SERVER
switchport access vlan 16
switchport mode access
```

Save your work

Switched Virtual Interface 

What is it?

It is a routed interface interface representing the IP addressing space for a particular VLAN connected to this interface.

1) Enable IP routing on CSW1

```
en
config t
ip routing

interface vlan 10
ip address 22.74.15.126 255.255.255.128
no shutdown

interface vlan 16
ip address 22.74.15.254 255.255.255.128
no shutdown

interface vlan 99
ip address 22.74.15.142 255.255.255.240
no shutdown

interface vlan 200
ip address 172.42.15.126 255.255.255.128
no shutdown

interface vlan 300
ip address 10.22.15.158 255.255.255.240
no shutdown
```