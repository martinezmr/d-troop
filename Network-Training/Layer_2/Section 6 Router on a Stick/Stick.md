### Router on a Stick

What does router on a stick even mean?

Bascially its a router that allows multiple vlans to talk when no layer-3 switches exist.

Step 1) Configure a basic hostname for the Router and Switch.

Step 2) Configure the subinterfaces using 802.1q encapsulation on the routers interfaces.

Step 3) Verify the ports are up and operational. Also verify your IPs' on the interfaces.

Step 4) Add VLANs to VLAN database on the switch and name them accordingly.

Step 5) Configure the trunk port on the switch and prune VLANs allowed to match sub-interfaces.

Step 6) Configure access ports to match the according VLAN to device.

Step 7) Verify port status

Step 8) Configure the VPCs with the proper IP address and ping their particular gateway.


### SOLUTION

## Router Commands
```
enable
config t
hostname R1

interface e0/0
no shut

interface Ethernet 0/0.10
encapsulation dot1q 10
ip address 192.168.10.126 255.255.255.128
no shutdown
exit
interface Ethernet 0/0.11
encapsulation dot1q 11
ip address 192.168.10.158 255.255.255.224
no shutdown
exit
interface Ethernet 0/0.16
encapsulation dot1q 16
ip address 192.168.10.190 255.255.255.224
no shutdown
end

show ip interface brief
```

## Switch Commands
```
en
config t
hostname SW1

vlan 10
name Users

vlan 11
name Management

vlan 16
name Servers

interface Ethernet 0/0
switchport trunk encapsulation dot1q
switchport mode trunk
switchport trunk allowed vlan 10,11,16
end

interface e1/0
switchport mode access
switchport access vlan 10

interface e0/3
switchport mode acceses
switchport access vlan 10

interface e0/2
switchport mode access
switchport access vlan 11

interface e0/1
switchport mode access
switchport access vlan 16

show interface status
```

## VPC 1
```
set pcname PC1
ip 192.168.10.1 /25 192.168.10.126
ping 192.168.10.126
```

## VPC 2
```
set pcname PC2
ip 192.168.10.2 /25 192.168.10.126
ping 192.168.10.126
```

## VPC3
```
set pcname PC3
ip address 192.168.10.129 /27 192.168.10.158
ping 192.168.10.158
```

## Server
```
set pcname Server
ip address 192.168.10.161 /27 192.168.10.190
ping 192.168.10.190
```

