## Execute the following Spanning-Tree 

What is Spanning-Tree? 

Its a layer 2 protocol that runs on switches and stuff we don't use. 

Main purpose of Spanning-Tree is to ensure that you do not create loops when you have redundant paths in your network.

Rapid Spanning-Tree replaced Spanning-Tree due to its faster convergence rate.



1) Configure all switches for Rapid PVST mode.

```
spanning-tree mode rapid-pvst
```

2) Manipulate 802.1 PVST parameters to ensure that predetermined switches take on the role of Spanning-Tree Root Bridge as follows.

Issue a command to ensure that Core Switch 1 is the Spanning-Tree Root Bridge for all VLANs.

* C100ACA253
```
spanning-tree vlan 10,16,99,200,300,1000 root primary
```


Issue a command to ensure that Core Switch 2 is the "backup" Spanning-Tree Root Bridge for all VLANs.

* C100ACB252
```
spanning-tree vlan 10,16,99,200,300,1000 root secondary
```

3) Verify your STP configuration on all switches and determine which ports are forwarding traffic. 

```
show spanning-tree
```

## On Access Switch 1 and 2 perform the following.

1) Configure all access ports with the following.
   > Spanning-Tree feature that will put the ports into the forwarding state as soon as an end station is conntected.

On both switches.
```
interface range e0/1-3
spanning-tree portfast 
```
   > Perform a Spanning-tree feature that will err-disable a port in which a BPDU is recieved on.

```
interface range e0/1-3
spanning-tree bpduguard enable
```



