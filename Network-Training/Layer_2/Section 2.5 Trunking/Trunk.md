## Configure all the inter-switch links as trunks.

* Using the text below as a guide.

```
interface ethernetx/x
description Trunk to C100XXX111
switchport trunk encapsulaton dot1q
switchport mode trunk
switchport trunk native vlan 1000
```

Verify your trunk configurations on all switches.

```
show interface trunk
```

All trunks

AAS249

```
en
config
interface e0/0
description Trunk to Distro 1
switchport trunk encapsulation dot1q
switchport mode trunk
switchport trunk native vlan 1000
```

AAS248
```
en
config
interface e0/0
description Trunk to Distro 2
switchport trunk encapsulation dot1q
switchport mode trunk
switchport trunk native vlan 1000
```

ADS251
```
en
config
interface e0/0
description Trunk to AWS1
switchport trunk encapsulation dot1q
switchport mode trunk
switchport trunk native vlan 1000

interface e0/1
description Trunk to CSW1
switchport trunk encapsulation dot1q
switchport mode trunk
switchport trunk native vlan 1000

interface e0/2
description Trunk to DSW1
switchport trunk encapsulation dot1q
switchport mode trunk
switchport trunk native vlan 1000

interface e0/3
description Trunk to r2
switchport trunk encapsulation dot1q
switchport mode trunk
switchport trunk native vlan 1000
```

ADS250
```
en
config
interface e0/0
description Trunk to AWS2
switchport trunk encapsulation dot1q
switchport mode trunk
switchport trunk native vlan 1000

interface e0/1
description Trunk to CSW2
switchport trunk encapsulation dot1q
switchport mode trunk
switchport trunk native vlan 1000

interface e0/2
description Trunk to DSW1
switchport trunk encapsulation dot1q
switchport mode trunk
switchport trunk native vlan 1000

interface e0/3
description Trunk to R1
switchport trunk encapsulation dot1q
switchport mode trunk
switchport trunk native vlan 1000
```

CSW1
```
interface e0/0
description Trunk to D1
switchport trunk encapsulation dot1q
switchport mode trunk
switchport trunk native vlan 1000

interface e0/1
description Trunk to D2
switchport trunk encapsulation dot1q
switchport mode trunk
switchport trunk native vlan 1000

interface e0/2
description Trunk to CSW2
switchport trunk encapsulation dot1q
switchport mode trunk
switchport trunk native vlan 1000

interface e0/3
description Trunk to CSW2
switchport trunk encapsulation dot1q
switchport mode trunk
switchport trunk native vlan 1000
```

CSW2
```
interface e0/0
description Trunk to D2
switchport trunk encapsulation dot1q
switchport mode trunk
switchport trunk native vlan 1000

interface e0/1
description Trunk to D1
switchport trunk encapsulation dot1q
switchport mode trunk
switchport trunk native vlan 1000

interface e0/2
description Trunk to CSW1
switchport trunk encapsulation dot1q
switchport mode trunk
switchport trunk native vlan 1000

interface e0/3
description Trunk to CSW1
switchport trunk encapsulation dot1q
switchport mode trunk
switchport trunk native vlan 1000
```

