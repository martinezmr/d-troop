# Perform these actions on all your switches.

Type in the command to show a brief summary of your ethernet ports.

```
show interface status
```

Configre a command so that if you mistype a command in the future, the switch will not attempt to perform a DNS resolution/lookup

```
no ip domain-lookup
```

Configure the switch with a command so when you're typing and a syslog messaged is displayed the switch will automatically repeat what you just typed.

```
line con 0
logging synchronous
```
Configure the switch so that it will forward any VTP messages recieved but not apply any changes to local VLANs.
-In transparent mode, you are able to create and modify and delete VLANs on the local switch, without affecting any other switches.

```
vtp mode transparent
```
Assign a hostname for each switch using enterprise standards. 
Hint 
2 Core switches / 2 Distro switches / 2 Access switches

```
C100ACA253
C100ACB252
C100ADS251
C100ADS250
C100AAS249
C100AAS248
```

Configure a username and password using the following credentials.

Username: admin

password: password
```
username admin secret password
```

# Configure a management interface and assign an IP as follows:

* Core Switch 1 = 10.22.15.253 /27
```
interface vlan 1000
ip address 10.22.15.253 255.255.255.224
no shutdown
```

* Core Switch 2 = 10.22.15.252 /27
```
interface vlan 1000
ip address 10.22.15.252 255.255.255.224
no shut
```

* Distro Switch 1 = 10.22.15.251 /27
```
int vlan 1000
ip address 10.22.15.251 255.255.255.224
no shut
```

* Distro Switch 2 = 10.22.15.250 /27
```
int vlan 1000
ip address 10.22.15.250 255.255.255.224
no shut
```

* Access Switch 1 = 10.22.15.249 /27
```
int vlan 1000
ip address 10.22.15.249 255.255.255.224
no shut
```

* Access Switch 2 = 10.22.15.248 /27
```
int vlan 1000
ip address 10.22.15.248 255.255.255.224
no shut
```

Configure the default gateway utilizing the highest usable IP address in the management network.

```
C100XXX111(config)ip default-gateway 10.22.15.254
```

Configure a localusername of admin and a secured password of password. Use a domain name of dtrp.us.

```
ip domain-name dtrp.us
```

Configure on your devices that HTTP and HTTPS web gui is disabled.
```
no ip http server
```

Configure a MOTD banner with the following text:
* UNAUTHORIZED ACCESS IS STRICTLY PROHIBITED!
```
banner motd ^
UNAUTHORIZED ACCESS IS STRICTLY PROHIBITED!
^
```

Ensure that the default SVI is shutdown.
* We shut down interface vlan 1 because it is by default for a management vlan.
```
interface vlan 1
shutdown
```

Configure the swithc that all passwords are encrypted when viewing the running configuration.
```
service password-encryption
```

Is your device displaying a big U or small u?

What casuses this? Why do we do it?

Defenition term

TACACs is a security application that provides centralized validation of users attempting to gain access to a device.

Basically do you have the right credentials to the network.

Configure TACACs on your devices for local login.

```
aaa new-model
aaa authentication login default local
```

SAVE YOUR WORK - its always a good habit to save your work so you're not like CRAP, and redo everything.
```
copy running-configuration startup-configuration
```
Baseline
```
hostname 

aaa new-model
aaa authentication login default local
username admin secret password
no ip domain-lookup
ip domain-name dtrp.us
service password-encryption
vlan 1000
name MGMT
ip default-gateway 10.22.15.254
interface vlan 1
shutdown
line con 0
logging synchronous
vtp mode transparent
no ip http server
banner motd ^
UNAUTHORIZED ACCESS IS STRICTLY PROHIBITED!
^
do wr
!
```